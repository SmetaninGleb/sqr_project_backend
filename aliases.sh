#!/bin/bash

set -o pipefail

# Context variables:
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

DC_FILE=${DC_FILE:-local.yml}
DC_FILE_FULL=$DIR/$DC_FILE

export COMPOSE_PROJECT_NAME=${PROJECT_NAME:-techmax}

# Check docker-compose file:
if ! test -f "$DC_FILE_FULL"; then
    echo "$DC_FILE_FULL does not exist!"
fi

# docker-compose aliases:
if [ -z "$PROJECT_NAME" ]; then
  alias dca="docker-compose -f '$DC_FILE_FULL'";
else
  alias dca="docker-compose -f '$DC_FILE_FULL' -p '$PROJECT_NAME'";
fi

alias dc_rm="dca run --rm "
alias dc_d="dca down"
alias dc_up="dca up --build"
alias dc_re="dc_d; dc_up"
alias dc_logs="dca logs -f"

# docker-compose django service:
alias dcd_rm="dc_rm django"
alias dcd_m="dcd_rm python manage.py"
alias dcd_shell="dcd_m shell_plus"
alias dcd_cov="dcd_rm coverage report"
alias dcd_t="dcd_rm coverage run -m pytest --settings=config.settings.test"

# tools
alias set_owner="sudo chown -R $USER . && chmod 775 -R ."
sef() {
  export "$(grep -v '^#' $1 | xargs -d '\n')"
}
alias pyclean='find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf' # Use: `py3clean .`

alias black_all='black --exclude "(migrations|\.direnv|\.eggs|\.git|\.hg|\.mypy_cache|\.nox|\.tox|\.venv|venv|\.svn|\.ipynb_checkpoints|_build|buck-out|build|dist|__pypackages__)" .'


_clean_json_file() {
  sed -i -n '/\[/,$p' $1;
  sed -i '/\]/{s/\(.*\]\).*/\1/;q}' $1;
  sed -i '/^$/d' $1;
}

dcd_bj() {
  # local _json_damp_file="${1/\./_}.json"
  local _json_damp_file="$DIR/dumps/${1/\./_}.json"
  dcd_m dumpdata --indent 4 $1 > $_json_damp_file
  _clean_json_file $_json_damp_file
}
