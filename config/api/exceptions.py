from rest_framework.exceptions import APIException


class TokenIsBlacklistedError(APIException):
    status_code = 403
    default_detail = "Token is blacklisted"
    default_code = "token_is_blacklisted"
