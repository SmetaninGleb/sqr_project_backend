from django.conf import settings
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter, SimpleRouter

# from config.api.views import logout_view
from techmax.galleries.api import views as galleries_views
from techmax.users.api.views import UserViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

# Users
router.register("users", UserViewSet)

# Galleries
router.register("galleries", galleries_views.GalleryViewSet, basename="galleries")
router.register("files", galleries_views.FileViewSet, basename="files")

app_name = "api"
urlpatterns = router.urls

urlpatterns += [
    # Login and logout
    path("auth-token/", obtain_auth_token),
    # path("token/logout/", logout_view, name="token_logout"),
]
