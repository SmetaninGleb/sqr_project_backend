from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.serializers import TokenRefreshSerializer
from rest_framework_simplejwt.tokens import RefreshToken

from config.api.exceptions import TokenIsBlacklistedError


@extend_schema(
    request=TokenRefreshSerializer,
    responses={
        status.HTTP_200_OK: None,
        status.HTTP_403_FORBIDDEN: TokenIsBlacklistedError,
    },
)
@api_view(("POST",))
def logout_view(request):
    """Blacklist the refresh token: extract token from the header
    during logout request user and refresh token is provided"""
    print("request.data: ", request.data)
    refresh_token = request.data["refresh"]
    try:
        token = RefreshToken(refresh_token)
        token.blacklist()
    except TokenError:
        raise TokenIsBlacklistedError()
    return Response(None, status=status.HTTP_200_OK)
