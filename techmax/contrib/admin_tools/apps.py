from django.apps import AppConfig


class AdminToolsConfig(AppConfig):
    name = "techmax.contrib.admin_tools"
    label = "admintools"
