from django.contrib import admin

from . import models


@admin.register(models.Gallery)
class GalleryAdmin(admin.ModelAdmin):
    list_display = ["owner", "name", "code", "description"]
    list_select_related = ["owner"]
    list_filter = ["owner", "name"]


@admin.register(models.File)
class FileAdmin(admin.ModelAdmin):
    list_display = ["owner", "file", "description"]
    list_select_related = ["owner"]
    list_filter = ["owner"]
