from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from ...users.api import serializers as users_serializers
from .. import models


class FileSerializer(serializers.ModelSerializer):
    """Serializer for File"""

    size = serializers.CharField(source="file.size", read_only=True)
    name = serializers.SerializerMethodField()
    extension = serializers.SerializerMethodField()
    # mime_type = serializers.CharField(source="file.content_type")

    class Meta:
        model = models.File
        fields = "__all__"
        read_only_fields = [
            "id",
            "owner",
            "size",
            "created_date",
            "updated_date",
        ]

    def get_name(self, instance):
        file = instance.file
        file_name = file.name.split("/")[1]
        return file_name

    def get_extension(self, instance):
        file = instance.file
        file_name = file.name.split(".")[1]
        return file_name

    def create(self, validated_data):
        file = super().create(validated_data)
        user_id = self.context.get("user_id")
        file.owner_id = user_id
        file.save(update_fields=["owner_id"])
        return file


class FilePartialUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.File
        fields = "__all__"
        extra_kwargs = {
            "file": {"required": False, "allow_null": True},
            "description": {"required": False, "allow_null": True},
        }
        read_only_fields = [
            "owner",
            "gallery",
            "created_date",
            "updated_date",
        ]


class GallerySerializer(serializers.ModelSerializer):
    """Serializer for Gallery"""

    files = FileSerializer(many=True, read_only=True)
    is_owner = serializers.SerializerMethodField(read_only=True)
    owner = users_serializers.UserSerializer(read_only=True)

    class Meta:
        model = models.Gallery
        fields = "__all__"
        read_only_fields = [
            "id",
            "files",
            "created_date",
            "updated_date",
            "owner",
            "code",
            "is_owner",
        ]

    def get_is_owner(self, instance):
        user = self.context["request"].user
        return instance.owner_id == user.id


class FileContentRequestSerializer(serializers.ModelSerializer):
    id = PrimaryKeyRelatedField(queryset=models.File.objects, many=False)

    class Meta:
        model = models.File
        fields = ["id"]


class FileContentResponseSerializer(serializers.ModelSerializer):
    file = serializers.FileField()

    class Meta:
        model = models.File
        fields = [
            "file",
        ]
