from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import OpenApiParameter, OpenApiResponse, extend_schema
from rest_framework import filters, mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from techmax.mixins.api.views import (
    ActionSerializerViewSetMixin,
    DisabledPutMethodMixin,
)

from .. import models
from ..services import generate_code
from . import serializers


class GalleryViewSet(
    ActionSerializerViewSetMixin,
    DisabledPutMethodMixin,
    mixins.CreateModelMixin,
    mixins.DestroyModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    """Gallery"""

    pagination_class = None
    filter_backends = [
        filters.OrderingFilter,
        DjangoFilterBackend,
        filters.SearchFilter,
    ]
    ordering_fields = ["id", "name"]
    ordering = "name"

    search_fields = ["name"]
    serializer_class = serializers.GallerySerializer
    queryset = models.Gallery.objects
    # serializer_action_classes = {
    #     "create": serializers.GalleryCreateSerializer,
    # }

    def get_queryset(self):
        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method." % self.__class__.__name__
        )
        return self.queryset.select_related("owner")

    def create(self, request, *args, **kwargs):
        """
        This endpoint creates gallery for the user
        """

        owner = request.user
        gallery_data = super().create(request, *args, **kwargs).data
        gallery_id = gallery_data.get("id")
        gallery = models.Gallery.objects.get(id=gallery_id)
        gallery.owner = owner
        code = generate_code(length=17)
        gallery.code = code
        gallery.save(update_fields=["owner", "code"])
        context = {"user_id": owner.pk, "request": request}
        serializer_data = serializers.GallerySerializer(gallery, context=context).data
        headers = self.get_success_headers(serializer_data)
        return Response(
            serializer_data, status=status.HTTP_201_CREATED, headers=headers
        )

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="code",
                type=str,
                location=OpenApiParameter.QUERY,
                required=False,
                description="Gallery code link",
            )
        ],
        responses={
            200: OpenApiResponse(
                response=serializers.GallerySerializer,
                description="Get gallery information by code",
            )
        },
    )
    @action(detail=False, methods=["GET"])
    def get_gallery_by_code(self, request):
        """
        Returns gallery info by its code
        """
        code = self.request.GET.get("code")
        gallery = models.Gallery.objects.get(code=code)
        context = {"request": request}
        serializer_data = serializers.GallerySerializer(gallery, context=context).data
        return Response(serializer_data)

    @extend_schema(
        responses={
            200: OpenApiResponse(
                response=serializers.GallerySerializer(many=True),
                description="Get current user's galleries",
            )
        },
    )
    @action(detail=False, methods=["GET"])
    def get_galleries_by_current_user(self, request):
        """
        Returns galleries info by owner_id
        """
        user = self.request.user
        galleries = models.Gallery.objects.filter(owner_id=user.pk)
        context = {"request": request}
        serializer_data = serializers.GallerySerializer(
            galleries, context=context, many=True
        ).data
        return Response(serializer_data)


class FileViewSet(
    ActionSerializerViewSetMixin, DisabledPutMethodMixin, viewsets.ModelViewSet
):
    """File"""

    pagination_class = None
    filter_backends = [
        filters.OrderingFilter,
        DjangoFilterBackend,
        filters.SearchFilter,
    ]

    ordering_fields = ["id"]

    serializer_class = serializers.FileSerializer
    queryset = models.File.objects.all()
    parser_classes = (MultiPartParser,)
    serializer_action_classes = {
        "partial_update": serializers.FilePartialUpdateSerializer,
    }

    def get_queryset(self):
        assert self.queryset is not None, (
            "'%s' should either include a `queryset` attribute, "
            "or override the `get_queryset()` method." % self.__class__.__name__
        )
        return self.queryset

    @extend_schema(
        request=serializers.FileContentRequestSerializer,
        responses={
            200: OpenApiResponse(
                response=serializers.FileContentResponseSerializer,
                description="Download file by file id",
            ),
        },
    )
    @action(detail=False, methods=["POST"])
    def download_file(self, request):
        """
        File can be downloaded using file_id
        """
        file_id = request.data.get("id")
        object_file = get_object_or_404(models.File, pk=file_id)
        if object_file:
            file_location = object_file.file.path
            file_name = object_file.file.name
            try:
                with open(file_location, "rb") as f:
                    file_data = f.read()

                response = HttpResponse(file_data, content_type="image/jpeg")
                response["Content-Disposition"] = f'attachment; filename="{file_name}"'

                return response
            except OSError:
                raise ValidationError("File does not exist")
