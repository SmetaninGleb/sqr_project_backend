from django.db import models
from django.utils.translation import gettext_lazy as _

from techmax.mixins.models import DatesModelMixin

from ..users import models as user_models


class Gallery(DatesModelMixin):
    owner = models.ForeignKey(
        user_models.User,
        verbose_name=_("Owner of the gallery"),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    name = models.TextField(verbose_name=_("Gallery name"))
    code = models.TextField(verbose_name=_("Generated gallery code"))
    description = models.TextField(null=True, blank=True, verbose_name=_("Description"))

    class Meta:
        verbose_name = _("Gallery")
        verbose_name_plural = _("Galleries")


class File(DatesModelMixin):
    owner = models.ForeignKey(
        user_models.User,
        verbose_name=_("Owner of the gallery"),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    file = models.FileField(verbose_name=_("File"), upload_to="files")
    gallery = models.ForeignKey(
        Gallery,
        verbose_name=_("Gallery"),
        related_name="files",
        on_delete=models.CASCADE,
    )
    description = models.TextField(
        null=True, blank=True, verbose_name=_("File description")
    )

    class Meta:
        verbose_name = _("File")
        verbose_name_plural = _("Files")
