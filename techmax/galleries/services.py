import random
import string


def generate_code(length=10):
    """Generate a random string of alphanumeric characters."""
    letters = string.ascii_lowercase + string.digits
    return "".join(random.choice(letters) for _ in range(length))
