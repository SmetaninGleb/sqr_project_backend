from django.core.files.uploadedfile import SimpleUploadedFile
from factory import Faker, SubFactory
from factory.django import DjangoModelFactory

from techmax.users.tests import factories as users_factories

from .. import models

file_content = b"test file content"
upload_file = SimpleUploadedFile("test.txt", file_content, content_type="text/plain")


class GalleryFactory(DjangoModelFactory):
    owner = SubFactory(users_factories.UserFactory)
    name = Faker("name")
    code = Faker("name")
    description = Faker("name")

    class Meta:
        model = models.Gallery


class FileFactory(DjangoModelFactory):
    description = Faker("name")
    gallery = SubFactory(GalleryFactory)
    file = upload_file
    owner = SubFactory(users_factories.UserFactory)

    class Meta:
        model = models.File
