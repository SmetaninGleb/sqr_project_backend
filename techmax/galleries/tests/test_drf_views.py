from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import RequestFactory
from rest_framework import status
from rest_framework.test import force_authenticate

from techmax.galleries.api import views
from techmax.users.tests.factories import get_user_model

from .. import models
from . import factories

User = get_user_model()


class TestGalleryViewSet:
    def test_get_queryset(self, user: User, rf: RequestFactory):
        view = views.GalleryViewSet()
        request = rf.get("/fake-url/")
        gallery = factories.GalleryFactory()
        request.user = user

        view.request = request

        assert gallery in view.get_queryset()

    def test_create(self, user: User, rf: RequestFactory):
        view = views.GalleryViewSet.as_view({"post": "create"})
        name = "Test Gallery"
        description = "descr"
        data = {
            "name": name,
            "description": description,
        }

        request = rf.post("/fake-url/", data=data, content_type="application/json")
        request.user = user
        force_authenticate(request, user=user)

        response = view(request)

        assert response.status_code == status.HTTP_201_CREATED
        assert models.Gallery.objects.filter().count() == 1
        gallery = models.Gallery.objects.get()
        assert gallery.name == name

    def test_partial_update(self, user: User, rf: RequestFactory):
        view = views.GalleryViewSet.as_view({"patch": "partial_update"})
        gallery = factories.GalleryFactory()

        new_name = "Test Gallery"
        data = {"name": new_name}

        request = rf.patch("/fake-url/", data=data, content_type="application/json")
        owner = gallery.owner
        request.user = owner
        force_authenticate(request, user=owner)
        response = view(request, pk=gallery.id)

        assert response.status_code == status.HTTP_200_OK
        assert models.Gallery.objects.filter().count() == 1
        gallery = models.Gallery.objects.get()
        assert gallery.name == new_name

    def test_list(self, user: User, rf: RequestFactory):
        view = views.GalleryViewSet.as_view({"get": "list"})
        request = rf.get("/fake-url/")

        gallery = factories.GalleryFactory()
        request.user = user
        force_authenticate(request, user=user)

        response = view(request)

        assert response.status_code == status.HTTP_200_OK
        assert response.data[0]["name"] == gallery.name
        assert response.data[0]["code"] == gallery.code

    def test_get_by_code(self, user: User, rf: RequestFactory):
        view = views.GalleryViewSet()

        gallery = factories.GalleryFactory()
        code = gallery.code

        request = rf.get("/fake-url/")
        request.user = user
        request.GET = {"code": code}
        view.request = request

        response = view.get_gallery_by_code(request)

        assert response.status_code == status.HTTP_200_OK
        assert response.data["id"] == gallery.id

    def test_get_by_user(self, user: User, rf: RequestFactory):
        view = views.GalleryViewSet()

        gallery = factories.GalleryFactory()

        owner = gallery.owner
        request = rf.get("/fake-url/")
        request.user = owner
        view.request = request

        response = view.get_galleries_by_current_user(request)

        assert response.status_code == status.HTTP_200_OK
        assert response.data[0]["id"] == gallery.id


class TestFileViewSet:
    def test_get_queryset(self, user: User, rf: RequestFactory):
        view = views.FileViewSet()
        request = rf.get("/fake-url/")
        file = factories.FileFactory()
        request.user = user

        view.request = request

        assert file in view.get_queryset()

    def test_create(self, user: User, rf: RequestFactory):
        view = views.FileViewSet.as_view({"post": "create"})
        gallery = factories.GalleryFactory()
        file_content = b"test file content"
        upload_file = SimpleUploadedFile(
            "test.txt", file_content, content_type="text/plain"
        )

        description = "descr"
        data = {
            "description": description,
            "owner": gallery.owner.pk,
            "gallery": gallery.pk,
        }

        request = rf.post("/fake-url/", data=data)
        request.FILES["file"] = upload_file
        request.user = user
        force_authenticate(request, user=user)

        response = view(request)

        assert response.status_code == status.HTTP_201_CREATED
        assert models.File.objects.filter().count() == 1
        file = models.File.objects.get()
        assert file.gallery == gallery
