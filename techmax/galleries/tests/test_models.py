from django.core.files.uploadedfile import SimpleUploadedFile

from techmax.users.models import User

from .. import models
from . import factories


class TestGallery:
    def test_create(self, user: User):
        models.Gallery.objects.create(
            owner_id=user.pk,
            name="Test gallery",
            code="TestCode",
        )
        assert models.Gallery.objects.count() == 1


class TestFile:
    def test_create(self, user: User):
        gallery = factories.GalleryFactory()
        file_content = b"test file content"
        upload_file = SimpleUploadedFile(
            "test.txt", file_content, content_type="text/plain"
        )

        models.File.objects.create(
            gallery_id=gallery.pk,
            owner_id=gallery.owner.id,
            file=upload_file,
        )
        assert models.File.objects.count() == 1
