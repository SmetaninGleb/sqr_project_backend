import pytest
from django.core.files.uploadedfile import SimpleUploadedFile

from techmax.galleries.api import serializers
from techmax.galleries.tests import factories
from techmax.users.models import User

from .. import models

pytestmark = pytest.mark.django_db


class TestGallerySerializer:
    def test_create(self, user: User):
        assert models.Gallery.objects.all().count() == 0

        data = {
            "name": "Test Gallery",
            "description": "My Gallery",
            "owner": user.pk,
            "code": "qwerty",
        }
        context = {"user_id": user.pk}
        serializer = serializers.GallerySerializer(data=data, context=context)
        assert serializer.is_valid(), "errors = " + str(serializer.errors)

        serializer.save()
        assert models.Gallery.objects.all().count() == 1


class TestFileSerializer:
    def test_create(self, user: User):
        gallery = factories.GalleryFactory()
        assert models.File.objects.all().count() == 0

        file_content = b"test file content"
        upload_file = SimpleUploadedFile(
            "test.txt", file_content, content_type="text/plain"
        )

        data = {
            "gallery": gallery.pk,
            "description": "My File",
            "file": upload_file,
            "owner": gallery.owner.pk,
        }
        serializer = serializers.FileSerializer(data=data)
        assert serializer.is_valid(), "errors = " + str(serializer.errors)

        serializer.save()
        assert models.File.objects.all().count() == 1
