from django.contrib import admin


class BaseDateInline(admin.TabularInline):
    readonly_fields = ["created_date", "updated_date"]
    extra = 0
