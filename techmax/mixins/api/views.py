from django.db.models.query import QuerySet


class FilterByUserViewSetMixin:
    def get_queryset(self):
        user = self.request.user
        return super().get_queryset().filter_by_user(user)


class DisabledPutMethodMixin:
    http_method_names = ["get", "post", "patch", "delete", "head", "options", "trace"]


class ActionSerializerViewSetMixin:
    """This class is intended to provide different serillizers for different types of requests."""

    serializer_action_classes = {}

    def get_serializer_class(self, *args, **kwargs):
        """Instantiate the list of serializers per action from class attribute (must be defined)."""
        #  kwargs["partial"] = True
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super().get_serializer_class(*args, **kwargs)


class ActionQuerysetViewSetMixin:
    queryset_action = {}

    def get_queryset(self, *args, **kwargs):
        try:
            queryset = self.queryset_action[self.action]
            if isinstance(queryset, QuerySet):
                # Ensure queryset is re-evaluated on each request.
                queryset = queryset.all()
            return queryset
        except (KeyError, AttributeError):
            return super().get_queryset(*args, **kwargs)


class ActionPermissionViewSetMixin:
    """
    Mixed permission base model allowing for action level
    permission control. Subclasses may define their permissions
    by creating a 'permission_classes_by_action' variable.

    Example:
    permission_classes_by_action = {'list': [AllowAny],
                                    'create': [IsAdminUser]}
    """

    permission_classes_by_action = {}

    def get_permissions(self, *args, **kwargs):
        try:
            return [
                permission()
                for permission in self.permission_classes_by_action[self.action]
            ]
        except (KeyError, AttributeError):
            return super().get_permissions(*args, **kwargs)
