from abc import ABC, abstractmethod

from django.contrib.auth import get_user_model
from django.db.models import QuerySet

User = get_user_model()


class AbstractFilterByUserQuerySet(ABC, QuerySet):
    @abstractmethod
    def filter_by_user(self, user: User) -> QuerySet:
        """Filter queryset by user"""


class DeleteQuerySet(QuerySet):
    def delete(self, *args, **kwargs):
        for obj in self:
            obj.delete()
        super().delete(*args, **kwargs)


class DeleteManagerMixin:
    def get_queryset(self):
        return DeleteQuerySet(model=self.model, using=self._db, hints=self._hints)
