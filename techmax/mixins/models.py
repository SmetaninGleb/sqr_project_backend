from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from . import managers


class CreatedDateModelMixin(models.Model):
    """Model mixin adds created_date field"""

    created_date = models.DateTimeField(_("Date of creation"), default=timezone.now)

    class Meta:
        abstract = True


class UpdateDateModelMixin(models.Model):
    """Model mixin adds updated_date field"""

    updated_date = models.DateTimeField(
        _("Update date"), auto_now=True, null=True, blank=True
    )

    class Meta:
        abstract = True


class DatesModelMixin(CreatedDateModelMixin, UpdateDateModelMixin):
    """Model mixin adds created_date and updated_date field"""

    class Meta:
        abstract = True


class DeleteModelMixin(models.Model):
    objects = managers.DeleteQuerySet.as_manager()

    class Meta:
        abstract = True
