from django.contrib.auth.password_validation import validate_password
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.validators import UniqueValidator

from .. import models


class UserSerializer(serializers.ModelSerializer):
    gender = serializers.ChoiceField(allow_blank=False, choices=models.Gender.choices)

    class Meta:
        model = models.User
        fields = [
            "id",
            "username",
            "last_name",
            "first_name",
            "patronymic",
            "email",
            "gender",
        ]


class UserMeSerializer(UserSerializer):
    class Meta(UserSerializer.Meta):
        fields = [
            "id",
            "username",
            "last_name",
            "first_name",
            "patronymic",
            "email",
            "gender",
            "is_superuser",
        ]


class UserCreateSerializer(serializers.ModelSerializer):
    id = PrimaryKeyRelatedField(read_only=True, many=False)
    email = serializers.EmailField(
        required=True,
        validators=[
            UniqueValidator(
                queryset=models.User.objects.all(),
                message=_("User with this email address already exists"),
            )
        ],
    )
    password = serializers.CharField(
        write_only=True, min_length=6, required=True, validators=[validate_password]
    )

    class Meta:
        model = models.User
        fields = [
            "id",
            "username",
            "email",
            "password",
        ]
