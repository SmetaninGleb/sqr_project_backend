from django.contrib.auth import get_user_model
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from techmax.mixins.api.views import (
    ActionPermissionViewSetMixin,
    ActionSerializerViewSetMixin,
    DisabledPutMethodMixin,
)

from . import serializers

User = get_user_model()


class UserViewSet(
    ActionSerializerViewSetMixin,
    ActionPermissionViewSetMixin,
    DisabledPutMethodMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    mixins.UpdateModelMixin,
    GenericViewSet,
):
    """User ViewSet"""

    filter_backends = [
        filters.OrderingFilter,
        DjangoFilterBackend,
        filters.SearchFilter,
    ]

    queryset = User.objects.all()
    lookup_field = "id"
    search_fields = ["first_name", "last_name", "patronymic", "email", "username"]

    serializer_class = serializers.UserSerializer
    serializer_action_classes = {
        "create": serializers.UserCreateSerializer,
    }

    # permission_classes = [IsAuthenticated]
    # permission_classes_by_action = {"create": [AllowAny]}

    @action(detail=False, serializer_class=serializers.UserMeSerializer)
    def me(self, request):
        serializer = serializers.UserMeSerializer(
            request.user, context={"request": request}
        )
        return Response(status=status.HTTP_200_OK, data=serializer.data)
