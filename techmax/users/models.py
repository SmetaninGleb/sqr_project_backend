from django.contrib.auth.models import AbstractUser
from django.db.models import CharField, TextChoices, TextField
from django.utils.translation import gettext_lazy as _


class Gender(TextChoices):
    MALE = "M", _("Мale")
    FEMALE = "F", _("Female")
    OTHER = "-", _("Other")


class User(AbstractUser):
    """
    Default custom user model for B2B Cloud Back.
    If adding fields that need to be filled at user signup,
    check forms.SignupForm and forms.SocialSignupForms accordingly.
    """

    #: First and last name do not cover name patterns around the globe
    first_name = TextField(null=True, blank=True, verbose_name=_("First name"))
    last_name = TextField(null=True, blank=True, verbose_name=_("Last name"))
    patronymic = TextField(null=True, blank=True, verbose_name=_("Patronymic"))
    gender = CharField(
        max_length=1,
        null=True,
        blank=True,
        choices=Gender.choices,
        verbose_name=_("Gender"),
    )
