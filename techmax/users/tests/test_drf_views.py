from django.test import RequestFactory

from techmax.users.api.views import UserViewSet
from techmax.users.models import User


class TestUserViewSet:
    def test_me(self, user: User, rf: RequestFactory):
        view = UserViewSet()
        request = rf.get("/fake-url/")
        request.user = user

        view.request = request

        response = view.me(request)

        assert response.data == {
            "username": user.username,
            "first_name": user.first_name,
            "gender": user.gender,
            "id": user.pk,
            "last_name": user.last_name,
            "email": user.email,
            "patronymic": user.patronymic,
            "is_superuser": True,
        }
