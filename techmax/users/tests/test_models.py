from .. import models


class TestUser:
    def test_create(self, db):
        models.User.objects.create(
            username="test@mail.ru",
            first_name="Name",
            last_name="Surname",
            gender=models.Gender.MALE,
            patronymic="Patronymic",
            email="test@mail.ru",
            password="test_pass",
        )
        assert models.User.objects.count() == 1
