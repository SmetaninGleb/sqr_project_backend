import pytest

from techmax.users.api import serializers

from .. import models

pytestmark = pytest.mark.django_db


class TestUserCreateSerializer:
    def test_simple_create(self):
        d = {
            "username": "test@example.com",
            "email": "test@example.com",
            "password": "Aa 123456789",
        }
        serializer = serializers.UserCreateSerializer(data=d)
        assert serializer.is_valid(), "errors = " + str(serializer.errors)

        serializer.save()

        assert models.User.objects.count() == 1

        user = models.User.objects.get()
        assert user.email == "test@example.com"
        assert user.username == "test@example.com"
